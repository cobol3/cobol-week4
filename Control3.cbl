       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CONTROL3.
       AUTHOR. NUTHJAREE.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  CITY-CODE   PIC   9  VALUE ZERO.
           88 CITY-IS-DUBLIN        VALUE 1.
           88 CITY-IS-LINERICK      VALUE 2.
           88 CITY-IS-CORK          VALUE 3.
           88 CITY-IS-GALWAY        VALUE 4.
           88 CITY-IS-SLIGO         VALUE 5.
           88 CITY-IS-WATERFORD     VALUE 6.
           88 UNIVERSITY-CITY       VALUE 1 THRU 4.
           88 CITY-IS-NOT-VALID     VALUE 0, 7, 8, 9.
       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "ENTER A CITY CODE (1-6) - " WITH NO ADVANCING 
           ACCEPT CITY-CODE 
           IF CITY-IS-NOT-VALID THEN
              DISPLAY "INVAILD CITY CODE ENTED"
           ELSE
              IF CITY-IS-LINERICK THEN
                 DISPLAY "HEY, WE'RE HOME."
               END-IF
               IF CITY-IS-DUBLIN THEN
                 DISPLAY "HEY,WE'RE IN THE CAPITAL" 
               END-IF
               IF UNIVERSITY-CITY THEN
                 DISPLAY "APPLY THE RENT SURARGE !" 
               END-IF  
           END-IF 
           SET CITY-IS-NOT-VALID  TO TRUE
           DISPLAY CITY-CODE
           GOBACK 
           .